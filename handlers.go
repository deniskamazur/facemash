package main

import (
	"encoding/json"
	"html/template"
	"log"
	http "net/http"
	"os/exec"
	"strconv"
	"strings"

	mw "./mongowrap"
)

// GetGirl get girl from vk by screenname
func GetGirl(screenname string) (girl mw.Girl, err error) {
	cmd := exec.Command("python3", append([]string{PYSCRIPT_PATH + "get_girl_by_vkid.py"}, screenname)...)
	bytes, err := cmd.Output()

	if err == nil {
		err = json.Unmarshal(bytes, &girl)
	}

	return girl, err
}

func getUserID(w http.ResponseWriter, r *http.Request) (id int) {
	sess, _ := cookiestore.Get(r, SESSION_CODE)

	if id, ok := sess.Values["id"]; ok {
		return id.(int)
	}

	id = dbw.GetNewUser().ID

	sess.Values["id"] = id
	sess.Save(r, w)

	return id
}

// Check if user's agent is a mobile agent
func isMobile(userAgent string) bool {
	userAgents := []string{"Android", "webOS", "iPhone", "BlackBerry", "Windows Phone"}

	for _, v := range userAgents {
		if strings.Contains(userAgent, v) {
			return true
		}
	}
	return false
}

// Page structs
type GirlPage struct {
	Girls   []mw.Girl
	IsAdmin bool
}

type RatingsPage struct {
	Girls   []mw.Girl
	IsAdmin bool
}

type AdminPage struct {
	IsAdmin   bool
	InviteKey mw.Key
}

type KeyPage struct {
	IsAdmin bool
}

type FAQPage struct {
	IsAdmin bool
}

type EditPage struct {
	IsAdmin bool
	Girl    mw.Girl
}

// HTTP handlers
func GirlHandler(w http.ResponseWriter, r *http.Request) {
	var gp GirlPage

	uid := getUserID(w, r)
	isadmin, err := dbw.IsUserAdmin(uid)
	gp.IsAdmin = isadmin

	if err != nil {
		log.Panic(err)
	}

	if r.Method == "GET" {
		var err error
		gp.Girls, err = dbw.GetRandomGirls(2)

		if err != nil {
			log.Panic(err)
		}
		if isMobile(r.Header.Get("User-Agent")) {
			template.Must(template.ParseFiles("templates/mobile/index.html", "templates/mobile/_header.html")).Execute(w, gp)
		} else {
			template.Must(template.ParseFiles("templates/desktop/index.html", "templates/desktop/_header.html")).Execute(w, gp)
		}

	} else {
		r.ParseForm()

		if vkid, ok := r.Form["id"]; ok {
			val, err := strconv.Atoi(vkid[0])

			if err == nil {
				dbw.IncWinsFor(val)
			} else {
				log.Panic(err)
			}

			http.Redirect(w, r, "/", 302)
		}
	}

}

func AdminHandler(w http.ResponseWriter, r *http.Request) {
	var ap AdminPage

	uid := getUserID(w, r)
	isadmin, err := dbw.IsUserAdmin(uid)
	ap.IsAdmin = isadmin

	if err != nil {
		log.Panic(err)
	}

	if r.Method == "GET" {
		if isadmin {
			r.ParseForm()
			if formID, ok := r.Form["form_id"]; ok && strings.EqualFold(formID[0], "get_key") {
				ap.InviteKey, err = dbw.GetRandomKey(uid)

				if err != nil {
					log.Panic(err)
				}
			}
		}
		if isMobile(r.Header.Get("User-Agent")) {
			template.Must(template.ParseFiles("templates/mobile/admin.html", "templates/mobile/_header.html")).Execute(w, ap)
		} else {
			template.Must(template.ParseFiles("templates/desktop/admin.html", "templates/desktop/_header.html")).Execute(w, ap)
		}
	} else {
		r.ParseForm()

		if url, ok := r.Form["url"]; ok {
			pieces := strings.Split(url[0], "/")
			scname := pieces[len(pieces)-1]

			girl, err := GetGirl(scname)

			if err == nil {
				err = dbw.AddGirl(girl)
				log.Println("Added girl: ", scname)
			} else {
				log.Panic(err)
			}
		}

		http.Redirect(w, r, "/admin", 302)
	}
}

func FAQHandeler(w http.ResponseWriter, r *http.Request) {
	var fp FAQPage

	uid := getUserID(w, r)
	isadmin, _ := dbw.IsUserAdmin(uid)
	fp.IsAdmin = isadmin

	if isMobile(r.Header.Get("User-Agent")) {
		template.Must(template.ParseFiles("templates/mobile/faq.html", "templates/mobile/_header.html")).Execute(w, fp)
	} else {
		template.Must(template.ParseFiles("templates/desktop/faq.html", "templates/desktop/_header.html")).Execute(w, fp)
	}
}

func RatingsHandler(w http.ResponseWriter, r *http.Request) {
	var rp RatingsPage

	uid := getUserID(w, r)
	isadmin, err := dbw.IsUserAdmin(uid)
	rp.IsAdmin = isadmin

	rp.Girls, err = dbw.GetTopGirls(30)

	if err != nil {
		log.Panic(err)
	}

	funcMap := template.FuncMap{
		"Add": func(a, b int) int { return a + b },
	}

	path := "templates/"
	if isMobile(r.Header.Get("User-Agent")) {
		path += "mobile/*"
	} else {
		path += "desktop/*"
	}

	tmpl := template.Must(template.New("stuff").Funcs(funcMap).ParseGlob(path))

	tmpl.ExecuteTemplate(w, "ratings.html", rp)
}

func KeyHandler(w http.ResponseWriter, r *http.Request) {
	//var kp KeyPage

	userID := getUserID(w, r)

	if r.Method == "GET" {
		url := strings.Split(r.URL.Path, "/")
		key := url[len(url)-1]

		err := dbw.GiveAdminPrivs(userID, key)

		if err != nil {
			log.Panic(err)
		} else {
			log.Printf("Successfully gave user %d admin priviligies!\n", userID)
		}

		http.Redirect(w, r, "/", 302)
	}
}

func EditHandler(w http.ResponseWriter, r *http.Request) {
	var ep EditPage

	uid := getUserID(w, r)
	isadmin, err := dbw.IsUserAdmin(uid)
	ep.IsAdmin = isadmin

	if err != nil {
		log.Panic(err)
	}

	if isadmin {
		r.ParseForm()

		if r.Method == "GET" {
			if id, ok := r.Form["id"]; ok {
				vkid, _ := strconv.Atoi(id[0])
				ep.Girl, err = dbw.GetGirlByVKId(vkid)

				if err != nil {
					log.Panic(err)
				}
			}
			if isMobile(r.Header.Get("User-Agent")) {
				template.Must(template.ParseFiles("templates/mobile/edit.html", "templates/mobile/_header.html")).Execute(w, ep)
			} else {
				template.Must(template.ParseFiles("templates/desktop/edit.html", "templates/desktop/_header.html")).Execute(w, ep)
			}

		} else {
			if formId, ok := r.Form["form_id"]; ok {
				if strings.EqualFold(formId[0], "delete") {
					if id, ok := r.Form["id"]; ok {
						vkid, _ := strconv.Atoi(id[0])
						dbw.DeleteGirl(vkid)

						log.Printf("Deleted girl %d\n", vkid)
						http.Redirect(w, r, "/", 302)
					}
				}
				if strings.EqualFold(formId[0], "update") {
					if id, ok := r.Form["id"]; ok {
						if photoURLs, ok := r.Form["photo_url"]; ok {
							vkid, _ := strconv.Atoi(id[0])
							err := dbw.UpdateGirlPhotoURLs(vkid, photoURLs)
							if err != nil {
								log.Panic(err)
							}

							log.Panicf("Updated girl %d photo to %s\n", vkid, photoURLs[0])

							http.Redirect(w, r, "/", 302)
						}
					}
				}
			}
		}
	}
}
