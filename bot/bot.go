package bot

import (
	"log"
	"math"
	"strings"

	strconv "strconv"

	"../mongowrap"
	vkapi "github.com/Dimonchik0036/vk-api"
)

type bot struct {
	db    *mongowrap.DBW
	token string
	admin int64
}

type claim struct {
	vkid int64
	url  string
}

//NewBot bot constructor
func NewBot(token string, admin int64, dbw *mongowrap.DBW) *bot {
	bot := new(bot)
	bot.db = dbw
	bot.token = token
	bot.admin = admin
	return bot
}

//Start start the bot
func (b bot) Start() {
	confirmations := make(map[string]claim)
	index := 0
	help := "1) set ссылка_на_фото (установить ваше фото на сайте) \n\n" +
		"	-если вы хотите скопировать ссылку на фото, то откройте ее в браузере (используйте фото с вашей страницы vk)," +
		"нажмите на неё ПКМ и нажмите на нужную кнопку в меню \n\n" +
		"	-после использования команды, будет отправленна только заявка на смену фото, его должен подтвердить администратор (вам прийдет оповещение) \n\n" +
		"2) delete (удалить вас из базы) \n\n" +
		"3) report сообщение (уведомить о баге/сообщение админу)\n\n"
	help_a := help +
		"3) confirm id (подтвердить заявку) \n\n" +
		"4) unconfirm id описание_отказа (отклонить заявку) \n\n" +
		"	-в описании отказа можно использовать пробелы"

	client, err := vkapi.NewClientFromToken(b.token)
	if err != nil {
		log.Panic(err)
	}
	if err := client.InitLongPoll(0, 2); err != nil {
		log.Panic(err)
	}
	updates, _, err := client.GetLPUpdatesChan(100, vkapi.LPConfig{25, vkapi.LPModeAttachments})
	if err != nil {
		log.Panic(err)
	}
	for update := range updates {
		if update.Message == nil || !update.IsNewMessage() || update.Message.Outbox() {
			continue
		}
		if strings.ToLower(update.Message.Text) == "delete" {
			b.db.DeleteGirl(int(update.Message.FromID))
			client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), "Мы удалили вас из базы."))
		}
		if strings.HasPrefix(strings.ToLower(update.Message.Text), "set") {
			if len(strings.Split(update.Message.Text, " ")) != 1 {
				index++
				confirmations[strconv.Itoa(index)] = claim{update.Message.FromID, strings.Split(update.Message.Text, " ")[1]}
				if index == math.MaxInt64 {
					index = -1
				}
				var info, err = client.UsersInfo(vkapi.NewDstFromUserID(update.Message.FromID))
				if err != nil {
					log.Panic(err)
				}
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(b.admin), "confirm link="+strings.Split(update.Message.Text, " ")[1]+" \n\nid="+strconv.Itoa(index)+
					" \n\nuser="+info[0].FirstName+" "+info[0].LastName+"( https://vk.com/id"+strconv.FormatInt(update.Message.FromID, 10)+")"))
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), "Заявка отправлена."))
			} else {
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), "Ошибка ввода."))
			}
		}
		if strings.ToLower(update.Message.Text) == "help" {
			if update.Message.FromID != b.admin {
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), help))
			} else {
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), help_a))
			}
		}
		if strings.HasPrefix(strings.ToLower(update.Message.Text), "confirm") && update.Message.FromID == b.admin {
			if len(strings.Split(update.Message.Text, " ")) > 1 {
				urls := []string{confirmations[strings.Split(update.Message.Text, " ")[1]].url}
				b.db.UpdateGirlPhotoURLs(int(update.Message.FromID), urls)
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(b.admin), "confirmed"))
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(confirmations[strings.Split(update.Message.Text, " ")[1]].vkid), "Ваша заявка подтверждена."))
				delete(confirmations, strings.Split(update.Message.Text, " ")[1])
			} else {
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(b.admin), "error"))
			}
		}
		if strings.HasPrefix(strings.ToLower(update.Message.Text), "unconfirm") && update.Message.FromID == b.admin {
			if len(strings.Split(update.Message.Text, " ")) > 1 {
				why := "\"неизвестно\""
				if len(strings.Split(update.Message.Text, " ")) > 2 {
					why = "\""
					for i := 2; i < len(strings.Split(update.Message.Text, " ")); i++ {
						why += strings.Split(update.Message.Text, " ")[i]
						if i != len(strings.Split(update.Message.Text, " "))-1 {
							why += " "
						}
					}
					why += "\""
				}
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(b.admin), "unconfirmed"))
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(confirmations[strings.Split(update.Message.Text, " ")[1]].vkid), "Ваша заявка отклонена. Причина: "+why))
				delete(confirmations, strings.Split(update.Message.Text, " ")[1])
			} else {
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(b.admin), "error"))
			}
		}
		if strings.HasPrefix(strings.ToLower(update.Message.Text), "report") {
			if len(strings.Split(update.Message.Text, " ")) > 1 {
				var about = "report:\""
				for i := 1; i < len(strings.Split(update.Message.Text, " ")); i++ {
					about += strings.Split(update.Message.Text, " ")[i]
					if i != len(strings.Split(update.Message.Text, " "))-1 {
						about += " "
					}
				}
				about += "\""
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), "Мы ценим вашу помощь.:)"))
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(b.admin), about))
			} else {
				client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), "К сожалению админ не умеет читать мысли.:("))
			}
		}
	}
}
