package mongowrap

import (
	"gopkg.in/mgo.v2/bson"
)

// User identified by Id
type User struct {
	ID      int  `bson:"id"`
	IsAdmin bool `bson:"is_admin"`
}

// IsUserAdmin tells if user has admin privelegies
func (dbw *DBW) IsUserAdmin(userID int) (bool, error) {
	var usr User
	err := dbw.Users.Find(bson.M{"id": userID}).One(&usr)

	return usr.IsAdmin, err
}

// GiveAdminPrivs gives admin priveleges to user with given ID
func (dbw *DBW) GiveAdminPrivs(userID int, keyVal string) (err error) {
	var key Key

	if dbw.Keys.Find(bson.M{"value": keyVal}).One(&key); key.CleatorID == userID {
		return nil
	}

	if valid, err := dbw.CheckKeyValidity(keyVal); valid {
		err = dbw.Users.Update(bson.M{"id": userID}, bson.M{"$set": bson.M{"is_admin": true}})

		return err
	}

	return &permissionError{}
}

// GetNewUser creates a new user
func (dbw *DBW) GetNewUser() User {
	id, _ := dbw.Users.Count()

	user := User{
		ID:      id,
		IsAdmin: false,
	}

	dbw.Users.Insert(user)

	return user
}
