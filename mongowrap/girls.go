package mongowrap

import (
	"fmt"
	"math/rand"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Girl - enough said
type Girl struct {
	Name      string   `bson:"name" json:"name"`
	PhotoURLs []string `bson:"photo_urls" json:"photo_urls"`
	VKId      int      `bson:"vkid" json:"uid"`
	Wins      int      `bson:"wins"`
}

type notEnoughPicsError struct {
	sliceLen int
	yourLen  int
}

func (e *notEnoughPicsError) Error() string {
	return fmt.Sprintf("%d bigger than %d", e.yourLen, e.sliceLen)
}

// GetRandomPhotoURL - return a random photo URL
func (g Girl) GetRandomPhotoURL() string {
	return g.PhotoURLs[rand.Intn(len(g.PhotoURLs))]
}

// DeleteGirl deletes a girl with given vkid
func (dbw *DBW) DeleteGirl(vkid int) error {
	return dbw.Girls.Remove(bson.M{"vkid": vkid})
}

// IncWinsFor increases the number of wins for a girl with a given vkid
func (dbw *DBW) IncWinsFor(vkid int) error {
	err := dbw.Girls.Update(bson.M{"vkid": vkid}, bson.M{"$inc": bson.M{"wins": 1}})

	return err
}

// GetRandomGirls returns a given number of random girls from the database
func (dbw *DBW) GetRandomGirls(size int) (girls []Girl, err error) {
	pipe := dbw.Girls.Pipe([]bson.M{{"$sample": bson.M{"size": size}}})
	err = pipe.All(&girls)

	return girls, err
}

// AddGirl adds a girl to database
func (dbw *DBW) AddGirl(girl Girl) error {
	if cnt, _ := dbw.Girls.Find(bson.M{"vkid": girl.VKId}).Count(); cnt == 0 {
		return dbw.Girls.Insert(girl)
	}
	return nil
}

// GetTopGirls return n girls with highest number of wins
func (dbw *DBW) GetTopGirls(size int) (girls []Girl, err error) {
	err = dbw.Girls.Find(struct{}{}).Sort("-wins").Limit(size).All(&girls)

	return girls, err
}

// GetGirlByVKId returns girl with given VKId
func (dbw *DBW) GetGirlByVKId(vkid int) (girl Girl, err error) {
	err = dbw.Girls.Find(bson.M{"vkid": vkid}).One(&girl)
	return girl, err
}

// UpdateGirlPhotoURLs updates girls photo
func (dbw *DBW) UpdateGirlPhotoURLs(vkid int, photoURLs []string) error {
	return dbw.Girls.Update(bson.M{"vkid": vkid}, bson.M{"$set": bson.M{"photo_urls": photoURLs}})
}

// NullifyRatings sets all girls wins to zero
func (dbw *DBW) NullifyRating() (*mgo.ChangeInfo, error) {
	return dbw.Girls.UpdateAll(struct{}{}, bson.M{"$set": bson.M{"wins": 0}})
}
