package mongowrap

import (
	"fmt"
	"math/rand"

	"gopkg.in/mgo.v2/bson"
)

// Key identified by value
type Key struct {
	CleatorID int    `bson:"creator_id"`
	Value     string `bson:"value"`
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

type permissionError struct {
}

func (e *permissionError) Error() string {
	return fmt.Sprintf("Permission denied")
}

// CheckKeyValidity tells if key with given value exists
func (dbw *DBW) CheckKeyValidity(val string) (status bool, err error) {
	if cnt, _ := dbw.Keys.Find(bson.M{"value": val}).Count(); cnt > 0 {
		err = dbw.Keys.Remove(bson.M{"value": val})

		return true, err
	}

	return false, err
}

// GetRandomKey creates and return a random key
func (dbw *DBW) GetRandomKey(providerID int) (key Key, err error) {
	if is, err := dbw.IsUserAdmin(providerID); is {
		key = Key{providerID, randStringRunes(6)}

		dbw.Keys.Insert(key)

		return key, err
	}

	return key, &permissionError{}
}
