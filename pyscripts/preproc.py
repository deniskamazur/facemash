def preprocess(user_dict):
    """
    Mostly getting rid of useless fields
    :param user_dict: vk respond dict w/ users
    :return: filtered dict
    """
    user = dict()

    user["vkid"] = user_dict["uid"]
    user["name"] = " ".join([user_dict["first_name"], user_dict["last_name"]])
    user["user_photos"] = [user_dict["photo_max_orig"]]
