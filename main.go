package main

import (
	"log"
	"net/http"

	vkbot "./bot"
	mw "./mongowrap"
	"github.com/gorilla/sessions"
	"gopkg.in/mgo.v2"
)

var info, _ = mgo.ParseURL(DBURI)
var sess, dberr = mgo.DialWithInfo(info)
var db = sess.DB(DBNAME)
var dbw = mw.NewDBW(db)

var bot = vkbot.NewBot(TOKEN, int64(ADMIN), dbw)

var cookiestore = sessions.NewCookieStore([]byte(COOKIE_STORE_KEY))

func main() {

	go bot.Start()

	http.HandleFunc("/", GirlHandler)
	http.HandleFunc("/faq/", FAQHandeler)
	http.HandleFunc("/admin/", AdminHandler)
	http.HandleFunc("/ratings/", RatingsHandler)
	http.HandleFunc("/keys/", KeyHandler)
	http.HandleFunc("/edit/", EditHandler)

	if dberr != nil {
		panic(dberr)
	}

	girls, err := dbw.GetTopGirls(5)

	if err == nil {
		for _, key := range girls {
			log.Println(key.Name)
		}
	} else {
		log.Panic(err)
	}

	key := mw.Key{0, "hello"}
	dbw.CheckKeyValidity("hello")
	dbw.Keys.Insert(key)

	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static", fs))

	err = http.ListenAndServe(PORT, nil)
	if err != nil {
		log.Panic(err)
	}
}
