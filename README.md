# Face Mash
## Requirements:
### Golang:
   * github.com/gorilla/sessions
   * gopkg.in/mgo.v2

### Python:
   * vkapi

## Running your own Face Mash server:
   1. Get yourself a mongodb server, you can get one free on https://mlab.com
   2. Create your python config file in pyscripts/config.py with vkontakte login data
   ```
   APP_ID   = "<your app name>" #not necassary
   LOGIN    = "<phone number/email>"
   PASSWORD = "<your password>"
   ```
   3. Create a golang config file called config.go 
   ```
   package main
   const (
        DBURI            = "mongodb://<your mongo db uri>" 
        DBNAME           = "gymmash"
        PORT             = ":5000"
        SESSION_CODE     = "gmash"
        COOKIE_STORE_KEY = "<a secret>"
        PYSCRIPT_PATH    = "./pyscripts/"
        )
   ```
   4. Install Requirements
   5. Run server with go run *.go

## Getting admin privileges:
On server startup move to localhost:5000/keys/hello, now you have admin privileges

## Suggesting features:
Feel free to suggest one in the issues!
